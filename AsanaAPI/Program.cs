﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Globalization;
using Export2Excel;
using System.Threading;

namespace AsanaAPI
{
    class Task
    {
        public string id { get; set; }
        public string name { get; set; }
        public string notes { get; set; }
        public string url { get; set; }
        public DateTime created_at { get; set; }
        public DateTime completed_at { get; set; }

        public Task() { }

        public Task(string id, string name, string notes, DateTime created_at, DateTime completed_at, string url)
        {
            this.id = id;
            this.name = name;
            this.notes = notes;
            this.created_at = created_at;
            this.completed_at = completed_at;
            this.url = url;
        }
    }


    class Asana
    {
        public string apiKey;
        public string projectId;

        void SetBasicAuthentication(WebRequest req)
        {
            var authInfo = apiKey + ":";
            var encodedAuthInfo = Convert.ToBase64String(
            Encoding.Default.GetBytes(authInfo));
            req.Headers.Add("Authorization", "Basic " + encodedAuthInfo);
        }

        public string GetTasks()
        {
            var req = WebRequest.Create("https://app.asana.com/api/1.0/projects/" + projectId + "/tasks?opt_fields=created_at,completed_at,completed,name,notes");
            // var req = WebRequest.Create("https://app.asana.com/api/1.0/projects//tasks?opt_fields=created_at&opt_fields=completed_at&opt_fields=name&opt_fields=notes&opt_fields=completed");
            SetBasicAuthentication(req);

            return new StreamReader(req.GetResponse().GetResponseStream()).ReadToEnd();
        }

        public Asana() { }

        public Asana(string api, string id)
        {
            this.apiKey = api;
            this.projectId = id;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {

            var ExcelOutputName = "Result.xlsx";

            DateTime cr_Test;
            DateTime cm_Test;

            Console.WriteLine("Please start datetime in format MM/dd/yyyy");
            while (true)
            {

                if (DateTime.TryParseExact(Console.ReadLine(), "MM/dd/yyyy", null, DateTimeStyles.None, out cr_Test) == true)
                {
                    Console.WriteLine("Please, input finish datetime in format MM/dd/yyyy");
                    while (true)
                    {
                        if (DateTime.TryParseExact(Console.ReadLine(), "MM/dd/yyyy", null, DateTimeStyles.None, out cm_Test) == true)
                        {
                            break;
                        }
                        else
                            Console.WriteLine("------------------------------------------");
                        Console.WriteLine("Sory, try again input finish project date in correct datetime format MM/dd/yyyy");
                    }
                    break;
                }
                else
                    Console.WriteLine("---------------------------------------");
                Console.WriteLine("Sory, try again input start project date in in correct datetime format MM/dd/yyyy");
            }


            Asana asana = new Asana(ConfigurationManager.AppSettings["API key"],
                ConfigurationManager.AppSettings["Project ID"]
                );

            JObject json = JObject.Parse(asana.GetTasks());

            List<Task> tasks = new List<Task>();

            for (int i = 0; i < json["data"].Count(); i++)
            {
                if ((bool)json["data"][i]["completed"])
                {
                    Task task = new Task(
                       json["data"].Children()["id"].ElementAt(i).ToString(),
                       json["data"].Children()["name"].ElementAt(i).ToString(),
                       json["data"].Children()["notes"].ElementAt(i).ToString(),
                       DateTime.Parse((json["data"].Children()["created_at"].ElementAt(i).ToString())),
                       DateTime.Parse((json["data"].Children()["completed_at"].ElementAt(i).ToString())),
                       "https://app.asana.com/0/" + asana.projectId + "/" +
                       json["data"].Children()["id"].ElementAt(i).ToString()
                       );

                    tasks.Add(task);
                }
            }

            List<Task> result = new List<Task>();

            foreach (var ta in tasks)
            {
                if ((ta.created_at > cr_Test.AddHours(23).AddMinutes(59)) && (ta.completed_at <= cm_Test.AddHours(23).AddMinutes(59)))
                {
                    result.Add(ta);
                }

            }

            var wb = new Workbook(ExcelOutputName).Worksheets.First();

            var headerTexts = "ID, Name, Notes, URL, Created_at, Completed_at".Split(',');
            var row = wb.NewRow(headerTexts.Length);
            var columnIndex = 0;

            foreach (var headerText in headerTexts)
                row.AddCell(columnIndex, headerTexts[columnIndex++]);

            foreach (var res in result)
            {
                row = wb.NewRow(headerTexts.Length);
                columnIndex = 0;
                row.AddCell(columnIndex++, res.id);
                row.AddCell(columnIndex++, res.name);
                row.AddCell(columnIndex++, res.notes);
                row.AddCell(columnIndex++, res.url);
                row.AddCell(columnIndex++, res.created_at.ToString());
                row.AddCell(columnIndex++, res.completed_at.ToString());
            }

            wb.Save();

            Console.WriteLine("Job is done");

            Console.WriteLine("Press enter to close...");
            Console.ReadLine();

        }
    }
}
